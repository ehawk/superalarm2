#include "SubsystemSound.hpp"

#include <fstream>
#include <iostream>

ExceptionSubsystemSound::ExceptionSubsystemSound(std::string msg)
{
	eMsg = msg;
}

ExceptionSubsystemSound::~ExceptionSubsystemSound()
{
	std::cout << "Destroyed!\n";
}

std::string ExceptionSubsystemSound::getErrorMsg()
{
	return eMsg;
}

std::string ExceptionSubsystemSound::getErrorDomain()
{
	return "ExceptionSubsystemSound";
}

DecoderWAV::DecoderWAV(std::string fileName)
{
	std::ifstream in(fileName.c_str());

	dwName = fileName;
	
	RIFFHeader header;
	in.read((char*)&header,sizeof(header));
	
	if (header.chunkMagic != 0x46464952)
		throw ExceptionSubsystemSound(std::string("Failed to read: ")+fileName);
		
	in.read((char*)&fmt,sizeof(WAVFormat));
	
	WAVSamples smplLength;
	in.read((char*)&smplLength,sizeof(WAVSamples));
	
	samples.reset(new char[smplLength.wavLength]);
	samplesCount = smplLength.wavLength;
	
	in.read(samples.get(),smplLength.wavLength);
	
	if (fmt.wavBitDepth == 16)
		fmt.wavFormat = SND_PCM_FORMAT_S16;
	else if (fmt.wavBitDepth == 8)
		fmt.wavFormat = SND_PCM_FORMAT_U8;
	
	if (in.bad())
		throw ExceptionSubsystemSound(std::string("Corrupt WAV: ")+fileName);
		
	done = false;
}

int DecoderWAV::getChannels()
{
	return fmt.wavChannels;
}

int DecoderWAV::getFrequency()
{
	return fmt.wavFrequency;
}

int DecoderWAV::getBitDepth()
{
	return fmt.wavBitDepth;
}

snd_pcm_format_t DecoderWAV::getFormat()
{
	return (snd_pcm_format_t) fmt.wavFormat;
}

char* DecoderWAV::getSamples(int offset, int size)
{
	if (offset+size < samplesCount)
	{
		return (samples.get()+offset);
	}
	else
	{
		end.reset(new char[size]);
		memset(end.get(),0,size);
		memcpy(end.get(),samples.get()+offset,samplesCount-offset);
		done = true;
		return end.get();
	}
}

bool DecoderWAV::isDone()
{
	return done;
}

std::string DecoderWAV::getName()
{
	return dwName;
}

DecoderWAV::~DecoderWAV()
{
	
}

SubsystemSound::SubsystemSound(Schedular* s)
{
	snd_pcm_t* tmp;
	if (snd_pcm_open(&tmp,"default",SND_PCM_STREAM_PLAYBACK, 0) < 0)
		throw ExceptionSubsystemSound("Failed to open sound device");
		
	if (s == 0)
		throw ExceptionSubsystemSound("A valid schedular is required");
		
	ssSchedular = s;
	ssHnd.reset(tmp);
	
	isPlaying = false;
}

void SubsystemSound::playWav(std::string wavFile)
{
	ssWav.reset(new DecoderWAV(wavFile));
	snd_pcm_set_params(ssHnd.get(),ssWav->getFormat(),SND_PCM_ACCESS_RW_INTERLEAVED,ssWav->getChannels(),ssWav->getFrequency(),1,500000);
	if (!isPlaying)
	{
		ssSchedular->addJob(this);
		isPlaying = true;
	}
	offset = 0;
}

SubsystemSound::~SubsystemSound()
{
	
}

struct pollfd SubsystemSound::getDescriptor()
{
	struct pollfd pfd;
	snd_pcm_poll_descriptors(ssHnd.get(),&pfd,1);
	return pfd;
}

bool SubsystemSound::onReady()
{
	snd_pcm_writei(ssHnd.get(),ssWav->getSamples(offset,PCM_PERIOD * (ssWav->getBitDepth()/8) * ssWav->getChannels()),PCM_PERIOD);
	offset += PCM_PERIOD * (ssWav->getBitDepth()/8) * ssWav->getChannels();
	if (ssWav->isDone())
	{
		isPlaying = false;
		snd_pcm_drop(ssHnd.get());
		return false;
	}
	return true;
}

std::string SubsystemSound::getName()
{
	return std::string("SubsystemSound: ")+ssWav->getName();
}

unsigned int SubsystemSound::getIdentifier()
{
	return 0;
}

bool SubsystemSound::isVolatile()
{
	return true;
}
