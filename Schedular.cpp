#include "Schedular.hpp"

#include <stdlib.h>
#include <string.h>
#include <iostream>

ExceptionSchedular::ExceptionSchedular(std::string msg)
{
	eMsg = msg;
}

std::string ExceptionSchedular::getErrorMsg()
{
	return eMsg;
}

std::string ExceptionSchedular::getErrorDomain()
{
	return "ExceptionSchedular";
}

Schedular::Schedular()
{
	pfd = 0;
	rebuildNeeded = false;
}

Schedular::~Schedular()
{	
	free(pfd);
}

void Schedular::addJob(IJob* job)
{
	if (job == nullptr)
		throw new ExceptionSchedular("(Schedular::addJob) Job can not be null");
	
	if (inLoop)
	{
		queuedJobs.push_back(std::unique_ptr<IJob>(job));
	}
	else
	{
		jobs.push_back(std::unique_ptr<IJob>(job));
	}
	rebuildNeeded = true;
}

void Schedular::removeJob(IJob* job)
{
	for (std::vector< std::unique_ptr<IJob> >::iterator i = jobs.begin(); i != jobs.end(); i++)
	{
		if ((*i)->getDescriptor().fd == job->getDescriptor().fd)
		{
			jobs.erase(i);
			rebuildNeeded = true;
			return;
		}
	}
}

void Schedular::removeJob(unsigned int id)
{
	for (std::vector< std::unique_ptr<IJob> >::iterator i = jobs.begin(); i != jobs.end(); i++)
	{
		if ((*i)->getIdentifier() == id)
		{
			jobs.erase(i);
			rebuildNeeded = true;
			return;
		}
	}
}

void Schedular::rebuildJobList()
{
	for (int i = 0; i < queuedJobs.size(); i++)
	{
		jobs.push_back(std::unique_ptr<IJob>(queuedJobs[i].release()));
	}
	
	queuedJobs.clear();
	
	pfd = (struct pollfd*) realloc(pfd,jobs.size()*sizeof(struct pollfd));
	int i = 0;
	for (std::vector< std::unique_ptr<IJob> >::iterator job = jobs.begin(); job != jobs.end(); job++)
	{
		struct pollfd newPfd = (*job)->getDescriptor();
		pfd[i].fd = newPfd.fd;
		pfd[i].events = newPfd.events;
		pfd[i].revents = newPfd.revents;
		i++;
	}
	
	rebuildNeeded = false;
}

bool Schedular::waitForJob()
{	
	if (rebuildNeeded)
		rebuildJobList();

	if (jobs.size() == 0)
		return false;

	int n = poll(pfd,jobs.size(),-1);
	
	inLoop = true;
	
	for (int i = 0; i < n; i++)
		runNextTask();
		
	inLoop = false;
		
	return true;
}

void Schedular::runNextTask()
{
	int i = 0;
	for (std::vector< std::unique_ptr<IJob> >::iterator job = jobs.begin(); job != jobs.end(); job++)
	{
		if (pfd[i].revents != 0)
		{
			pfd[i].revents = 0;
			if (!(*job)->onReady())
			{
				if ((*job)->isVolatile())
					(*job).release();
				jobs.erase(job);
				rebuildNeeded = true;
			}
			return;
		}
		i++;
	}
}
