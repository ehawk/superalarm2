#include "SuperAlarm.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <memory.h>
#include <dirent.h>
#ifdef USE_NOTIFY
#include <libnotify/notify.h>
#endif

SuperAlarm::SuperAlarm()
{
	saINI.reset(new ParserINI("config.ini"));
	saSchedular.reset(new Schedular());
	saSysSnd.reset(new SubsystemSound(saSchedular.get()));
}

SuperAlarm::~SuperAlarm()
{
	//The schedular will delete the network handler, destroying it here will cause a sigsegv
	saSysNet.release();
	//same thing
	saSysSnd.release();
}

void SuperAlarm::begin()
{
	setupConfig();	
	while (1) { if(!saSchedular->waitForJob()) break;}
}

void SuperAlarm::scheduleAlarm(Section& sect)
{
	try
	{
		int timeTillElapse = std::stoi(sect.getProperty("EPOCH")->getValue())-time(0);
		int state = std::stoi(sect.getProperty("STATE")->getValue());
		if (timeTillElapse > 0 && state == STATE_ACTIVE)
		{
			saSchedular->addJob(new JobAlarm(sect,this));
			std::cout << "Adding alarm: " << sect.getProperty("NAME")->getValue() << "\n";
		}
		else
			std::cout << "Ignoring alarm: " << sect.getProperty("NAME")->getValue() << "\n";
	}
	catch (...)
	{
		std::cout << "Invalid alarm found, ignoring... (index " << sect.getUID() <<")\n";
	}
}

void SuperAlarm::setupConfig()
{
	try
	{
		saINI->parse();
		
		uint16_t porti;
		
		try
		{
			int portw = std::stoi(saINI->getProperty("CONF","PORT"));
			if (portw < 0 || portw > 65536)
			{
				std::cout << "Using default port: 5991, port in config.ini is out of range\n";
				porti = 5991;
			}
			else
				porti = portw;
		}
		catch (ExceptionINI& e)
		{
			std::cout << "Using default port: 5991, change in config.ini\n";
			porti = 5991;
		}
		
		saSysNet.reset(new SubsystemNetwork(saSchedular.get(),this,porti));
		
		std::vector<Section> sects(saINI->getSections("ALARM"));
		for (int i = 0; i < sects.size(); i++)
		{
			scheduleAlarm(sects[i]);
		}
		
		saSysNet->enable();
	}
	catch (ExceptionINI& e)
	{
		std::cout << "[" << e.getErrorDomain() << "]: " << e.getErrorMsg() << "\n";
		std::cout << "Bad config, must exit\n";
	}
}

void SuperAlarm::CommandAddAlarm(uint8_t* newAlarm)
{
	AlarmDef* alarmDef = (AlarmDef*) newAlarm;
	char* alarmLabel = (char*) (newAlarm+16);
	uint32_t uid = 0;
	
	if (alarmDef->Identifier == 0xFFFFFFFF) //new alarm
	{
		uid = saINI->addSection("ALARM");
		saINI->addProperty("ALARM","NAME",std::string(alarmLabel,(alarmDef->NameLength)),uid);
		saINI->addProperty("ALARM","EPOCH",std::string()+=std::to_string(alarmDef->Epoch),uid);
		saINI->addProperty("ALARM","PERIOD",std::string()+=std::to_string(alarmDef->Period),uid);
		saINI->addProperty("ALARM","STATE",std::string()+=std::to_string(alarmDef->State),uid);
		saINI->addProperty("ALARM","TYPE",std::string()+=std::to_string(alarmDef->Type),uid);
		saINI->addProperty("ALARM","ACTION",std::string()+=std::to_string(alarmDef->Action),uid);
		saINI->addProperty("ALARM","ID",std::string()+=std::to_string(uid),uid);
		saINI->writeChanges();
	}
	else //edit existing alarm
	{
		saINI->removeSection("ALARM",saINI->getSectionByProperty("ID",std::to_string(alarmDef->Identifier))->getUID());
		saSchedular->removeJob(alarmDef->Identifier);
		
		uid = saINI->addSection("ALARM");
		saINI->addProperty("ALARM","NAME",std::string(alarmLabel,(alarmDef->NameLength)),uid);
		saINI->addProperty("ALARM","EPOCH",std::string()+=std::to_string(alarmDef->Epoch),uid);
		saINI->addProperty("ALARM","PERIOD",std::string()+=std::to_string(alarmDef->Period),uid);
		saINI->addProperty("ALARM","STATE",std::string()+=std::to_string(alarmDef->State),uid);
		saINI->addProperty("ALARM","TYPE",std::string()+=std::to_string(alarmDef->Type),uid);
		saINI->addProperty("ALARM","ACTION",std::string()+=std::to_string(alarmDef->Action),uid);
		saINI->addProperty("ALARM","ID",std::string()+=std::to_string(uid),uid);
		saINI->writeChanges();
	}
	
	scheduleAlarm(*saINI->getSectionBySUID(uid));
	
	CommandGetAlarm(&uid);
}

void SuperAlarm::CommandDeleteAlarm(uint32_t* index)
{
	try
	{
		
		saINI->removeSection("ALARM",saINI->getSectionByProperty("ID",std::to_string(*index))->getUID());
		saINI->writeChanges();
		//std::cout << "DeleteAlarm: " << *index << "\n";
		
		saSchedular->removeJob(*index);
		
		int s = 1;
		
		saSysNet->sendPacket((uint8_t*)&s,4,CMD_DELETEID);
	}
	catch(ExceptionINI& e)
	{
		int s = 0;
		std::cout << "Can't delete non-existant alarm: " << *index << "\n";
		saSysNet->sendPacket((uint8_t*)&s,4,CMD_DELETEID);
	}
}

void SuperAlarm::CommandGetAlarm(uint32_t* index)
{
	std::cout << "Command: GetAlarm, index: " << *index << "\n";
	Section* sect = saINI->getSectionByProperty("ID",std::to_string(*index));
	
	try
	{
		AlarmDef alarmDef;
		alarmDef.Epoch = std::stoi(sect->getProperty("EPOCH")->getValue());
		alarmDef.Period = std::stoi(sect->getProperty("PERIOD")->getValue());
		alarmDef.Identifier = *index;
		alarmDef.State = std::stoi(sect->getProperty("STATE")->getValue());
		alarmDef.Type = std::stoi(sect->getProperty("TYPE")->getValue());
		alarmDef.Action = std::stoi(sect->getProperty("ACTION")->getValue());
		alarmDef.NameLength = sect->getProperty("NAME")->getValue().length();
		
		char buffer[16+alarmDef.NameLength];
		
		memcpy(buffer,&alarmDef,16);
		memcpy(buffer+16,sect->getProperty("NAME")->getValue().c_str(),alarmDef.NameLength);
		
		saSysNet->sendPacket((uint8_t*)buffer,16+alarmDef.NameLength,CMD_GET);
	}
	catch (ExceptionINI& e)
	{
		std::cout << "Request for invalid alarm, ignoring..\n";
		saSysNet->sendPacket(0,0,CMD_RESET);
	}
}

void SuperAlarm::CommandListAlarms()
{
	std::vector<Section> sections = saINI->getSections("ALARM");
	std::vector<AlarmDef> alarms;
	int totalSize = 0;
	
	for (int i = 0; i < sections.size(); i++)
	{
		AlarmDef a;
		a.Epoch = std::stoi(sections[i].getProperty("EPOCH")->getValue());
		a.Period = std::stoi(sections[i].getProperty("PERIOD")->getValue());
		a.Identifier = std::stoi(sections[i].getProperty("ID")->getValue());
		a.State = std::stoi(sections[i].getProperty("STATE")->getValue());
		a.Type = std::stoi(sections[i].getProperty("TYPE")->getValue());
		a.Action = std::stoi(sections[i].getProperty("ACTION")->getValue());
		a.Name = (uint8_t*)sections[i].getProperty("NAME")->getValue().c_str();
		a.NameLength = sections[i].getProperty("NAME")->getValue().length();
		alarms.push_back(a);
		totalSize+=16+a.NameLength;
	}
	
	int offset = 0;
	char buffer[totalSize];
	
	for (int i = 0; i < alarms.size(); i++)
	{
		memcpy(buffer+offset,&alarms[i],16);
		offset+=16;
		memcpy(buffer+offset,alarms[i].Name,alarms[i].NameLength);
		offset+=alarms[i].NameLength;
	}
	
	saSysNet->sendPacket((uint8_t*)buffer,totalSize,CMD_LISTALARMS);
}

void SuperAlarm::CommandListSounds()
{
	DIR* directory;
	struct dirent* entry;
	
	try
	{
		directory = opendir(saINI->getProperty("CONF","SOUNDPATH").c_str());
	}
	catch (ExceptionINI& e)
	{
		std::cout << "No sound directory set, using run dir...\n";
		directory = opendir(".");
	}
	
	if (directory == 0)
	{
		std::cout << "Path set is invalid, using run dir...\n";
		directory = opendir(".");
	}
	
	std::list<std::string> dirList;
	int totalSize = 0;
	while ((entry = readdir(directory)) != 0)
	{
		if (entry->d_name[0] == '.')
			continue;
		
		dirList.push_back(std::string(entry->d_name));
		totalSize += dirList.back().length();
	}
	
	char buffer[totalSize+dirList.size()+4];
	
	uint32_t count = dirList.size();
	memcpy(buffer,&count,4);
	
	std::cout << "Count: " << *(uint32_t*)buffer << " Size: " << totalSize+dirList.size() << "\n";
	
	unsigned int offset = 4;
	
	for (std::list<std::string>::iterator i = dirList.begin(); i != dirList.end(); i++)
	{
		std::string& s = (*i);
		buffer[offset++] = s.length();
		memcpy(buffer+offset,s.c_str(),s.length());
		offset += s.length();
	}
	
	//std::cout << "Buffer: " << buffer << "\n";
	
	saSysNet->sendPacket((uint8_t*)buffer,totalSize+dirList.size()+4,CMD_LISTSOUND);
	
	closedir(directory);
}

void SuperAlarm::CommandSetSound(uint8_t* data)
{
	uint32_t* 	suid = (uint32_t*)data;
	uint8_t*	fileLength = (data+4);
	char*	fileName = (char*)(data+5);
	
	saINI->addProperty("ALARM","SOUND",std::string(fileName,*fileLength),saINI->getSectionByProperty("ID",std::to_string(*suid))->getUID());
	saINI->writeChanges();
}

void SuperAlarm::onExpire(std::string name, unsigned int idx)
{
	try
	{
		unsigned int id = saINI->getSectionByProperty("ID",std::to_string(idx))->getUID();
		
		int action = std::stoi(saINI->getProperty("ALARM","ACTION",id));
		
		if (action == ACTION_SOUND)
		{
			try
			{
				saSysSnd->playWav(saINI->getProperty("CONF","SOUNDPATH")+"/"+saINI->getProperty("ALARM","SOUND",id));
			}
			catch (Exception& e)
			{
				std::cout << "[" << e.getErrorDomain() << "] " << e.getErrorMsg() << "\n";
				std::cout << "Failed to play alarm sound\n";
			}
		}
		else if (action == ACTION_NOTIFY)
		{
			#ifdef USE_NOTIFY
			notify_init ("ACTION_NOTIFY");
			NotifyNotification* notify = notify_notification_new (name.c_str(), "Alarm Expired", "dialog-information");
			notify_notification_show (notify, NULL);
			g_object_unref(G_OBJECT(notify));
			notify_uninit();
			#else
			std::cout << "Notify support not enabled\n";
			#endif
		}
		else
		{
			std::cout << "Unknown trigger action: " << action << ", failed.\n";
		}
		
		if (std::stoi(saINI->getProperty("ALARM","TYPE",id)) == 0)
		{
			saINI->removeSection("ALARM",id);
			saINI->writeChanges();
		}
	}
	catch (Exception& e)
	{
		std::cout << "[" << e.getErrorDomain() << "] " << e.getErrorMsg() << "\n";
		std::cout << "Exception in trigger event, failed\n";
	}
}

int main(int argc, char** argv)
{
	try
	{
		SuperAlarm sa;
		sa.begin();
	}
	catch (Exception& e)
	{
		std::cout << "[" << e.getErrorDomain() << "]: " << e.getErrorMsg() << "\n";
	}
	catch (...)
	{
		std::cout << "Unknown exception, must exit\n";
		throw;
	}
}
