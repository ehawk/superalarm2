#ifndef _EXCEPTION_HPP_
#define _EXCEPTION_HPP_

#include <string>

class Exception
{
	public:
		virtual std::string getErrorMsg()
		{
			return "Unknown Exception";
		}
			
		virtual std::string getErrorDomain()
		{
			return "Exception";
		}
};

#endif
