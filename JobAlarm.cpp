#include <sys/timerfd.h>
#include <iostream>
#include "JobAlarm.hpp"
#include "SubsystemNetwork.hpp"

JobAlarm::JobAlarm(Section& sect, IAlarmCallback* callback)
{
	aType = std::stoi(sect.getProperty("TYPE")->getValue());
	aID = std::stoi(sect.getProperty("ID")->getValue());
	aName = sect.getProperty("NAME")->getValue();
	aCallback = callback;
	aPeriod = std::stoi(sect.getProperty("PERIOD")->getValue());
	
	timeToExpire.it_value.tv_sec = std::stoi(sect.getProperty("EPOCH")->getValue())-time(0);
	timeToExpire.it_value.tv_nsec = 0;
	timeToExpire.it_interval.tv_sec = 0;
	timeToExpire.it_interval.tv_nsec = 0;
	
	clockDescriptor = timerfd_create(CLOCK_MONOTONIC,TFD_NONBLOCK);
	timerfd_settime(clockDescriptor,0,&timeToExpire,0);
	
	std::cout << "Period: " << aPeriod << "\n";
}

JobAlarm::~JobAlarm()
{
	std::cout << "JobAlarm Destructor!\n";
}

struct pollfd JobAlarm::getDescriptor()
{
	struct pollfd pfd;
	pfd.fd = clockDescriptor;
	pfd.events = POLLIN;
	pfd.revents = 0;
	
	return pfd;
}

bool JobAlarm::onReady()
{
	std::cout << "Expired: " << aName << "\n";
	if (aCallback != nullptr)
		aCallback->onExpire(aName,aID);
	else
		return false;
	if (aType == TYPE_CONTINUOUS)
	{	
		timeToExpire.it_value.tv_sec = aPeriod;
		timeToExpire.it_value.tv_nsec = 0;
		timeToExpire.it_interval.tv_sec = 0;
		timeToExpire.it_interval.tv_nsec = 0;
		
		timerfd_settime(clockDescriptor,0,&timeToExpire,0);
		
		return true;
	}
	return false;
}

std::string JobAlarm::getName()
{
	return "JobAlarm";
}

unsigned int JobAlarm::getIdentifier()
{
	return aID;
}

bool JobAlarm::isVolatile()
{
	return false;
}
