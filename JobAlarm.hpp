#ifndef _JOBALARM_HPP_
#define _JOBALARM_HPP_

#include "Schedular.hpp"
#include "ParserINI.hpp"

#include <sys/timerfd.h>
#include <string>

class IAlarmCallback
{
	public:
		virtual void onExpire(std::string name, unsigned int id) = 0;
};

class JobAlarm : public IJob
{
	public:
		JobAlarm(Section& sect, IAlarmCallback* callback);
		~JobAlarm();
		struct pollfd getDescriptor();
		bool onReady();
		std::string getName();
		unsigned int getIdentifier();
		bool isVolatile();
		
	private:
		int clockDescriptor;
		struct itimerspec timeToExpire;
		std::string aName;
		unsigned int aID;
		unsigned int aPeriod;
		char aType;
		IAlarmCallback* aCallback;
};

#endif
