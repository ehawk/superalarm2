#ifndef _SUPER_ALARM_HPP_
#define _SUPER_ALARM_HPP_

#include "Exception.hpp"
#include "ParserINI.hpp"
#include "Schedular.hpp"
#include "JobAlarm.hpp"
#include "SubsystemSound.hpp"
#include "SubsystemNetwork.hpp"

#include <string>
#include <memory>

class SuperAlarm : public IAlarmCallback, public ICommandInterface
{
	public:
		void onExpire(std::string name, unsigned int id);
		SuperAlarm();
		~SuperAlarm();
		void begin();
		
		void CommandAddAlarm(uint8_t* newAlarm);
		void CommandDeleteAlarm(uint32_t* index);
		void CommandGetAlarm(uint32_t* index);
		void CommandListAlarms();
		void CommandListSounds();
		void CommandSetSound(uint8_t* data);
		
	private:
		std::unique_ptr<ParserINI> saINI;
		std::unique_ptr<Schedular> saSchedular;
		std::unique_ptr<SubsystemSound> saSysSnd;
		std::unique_ptr<SubsystemNetwork> saSysNet;
		
		void scheduleAlarm(Section& sect);
		void setupConfig();
};

#endif
