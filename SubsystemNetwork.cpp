#include "SubsystemNetwork.hpp"


#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

ExceptionSubsystemNetwork::ExceptionSubsystemNetwork(std::string msg)
{
	esnMsg = msg;
}

std::string ExceptionSubsystemNetwork::getErrorMsg()
{
	return esnMsg;
}

std::string ExceptionSubsystemNetwork::getErrorDomain()
{
	return "ExceptionSubsystemNetwork";
}

SubsystemNetwork::SubsystemNetwork(Schedular* s, ICommandInterface* com, int port)
{
	snListener = socket(AF_INET,SOCK_STREAM, 0);
	struct in_addr ipAddr;
		ipAddr.s_addr = INADDR_ANY;
	struct sockaddr_in serverAddress;
		serverAddress.sin_family = AF_INET;
		serverAddress.sin_port = htons(port);
		serverAddress.sin_addr = ipAddr;
	bind(snListener,(struct sockaddr*)&serverAddress,(socklen_t)sizeof(struct sockaddr_in));
	listen(snListener,1);
	
	snSchedular = s;
	snCom = com;
}

SubsystemNetwork::~SubsystemNetwork()
{
	std::cout << "SubSystemNetwork ended\n"; 
	close(snListener);
}

void SubsystemNetwork::sendPacket(uint8_t* data, uint16_t size, int command)
{
	if(snClientSocket == 0)
		throw ExceptionSubsystemNetwork("A packet can only be sent when an active connection is opened");
	
	char packBuffer[16+size];
	Packet* packet = (Packet*)packBuffer;
	packet->Magic = MAGIC_NUMBER;
	packet->Command = command|1;
	packet->Flags = 0;
	packet->Size = size;

	memcpy(packBuffer+16,data,size);

	write(snClientSocket,(char*)packBuffer,size+16);
}

void SubsystemNetwork::enable()
{
	snSchedular->addJob(this);
}

struct pollfd SubsystemNetwork::getDescriptor()
{
	struct pollfd pfd;
	pfd.fd = snListener;
	pfd.events = POLLIN;
	return pfd;
}

bool SubsystemNetwork::onReady()
{
	struct sockaddr_in clientInfo;
	socklen_t clientInfoSize = sizeof(struct sockaddr_in);
	
	snClientSocket = accept(snListener,(struct sockaddr*)&clientInfo,&clientInfoSize);
	
	std::cout << "Conection\n";
	
	Packet packet;
	
	read(snClientSocket,&packet,16);
	
	if (packet.Magic == MAGIC_NUMBER)
		std::cout << "ALRM Packet\n";
	else
		return true;
	
	packet.Data = new uint8_t[packet.Size];
	read(snClientSocket,packet.Data,packet.Size);
	
	try
	{
		switch(packet.Command)
		{
			case CMD_ADDALARM:
				snCom->CommandAddAlarm(packet.Data);
				break;
			case CMD_GET:
				snCom->CommandGetAlarm((uint32_t*)packet.Data);
				break;
			case CMD_LISTALARMS:
				snCom->CommandListAlarms();
				break;
			case CMD_DELETEID:
				snCom->CommandDeleteAlarm((uint32_t*)packet.Data);
				break;
			case CMD_LISTSOUND:
				snCom->CommandListSounds();
				break;
			case CMD_SETSOUND:
				snCom->CommandSetSound(packet.Data);
				break;
		}
	}
	catch (ExceptionSubsystemSound& e)
	{
		std::cout << "[" << e.getErrorDomain() << "]: " << e.getErrorMsg() << "\n";
	}
	catch (ExceptionSchedular& e)
	{
		std::cout << "[" << e.getErrorDomain() << "]: " << e.getErrorMsg() << "\n";
	}
	catch (ExceptionINI& e)
	{
		std::cout << "[" << e.getErrorDomain() << "]: " << e.getErrorMsg() << "\n";
	}
	catch (ExceptionSubsystemNetwork& e)
	{
		std::cout << "[" << e.getErrorDomain() << "]: " << e.getErrorMsg() << "\n";
	}
	catch(...)
	{
		throw ExceptionSubsystemNetwork("Error while handling command");
	}
	
	delete [] packet.Data;
	
	close(snClientSocket);
	snClientSocket = 0;
	
	return true;
}

std::string SubsystemNetwork::getName()
{
	return "SubsystemNetwork";
}

unsigned int SubsystemNetwork::getIdentifier()
{
	return 0;
}

bool SubsystemNetwork::isVolatile()
{
	return true;
}
