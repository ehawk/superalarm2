#include "ParserINI.hpp"

#include <fstream>
#include <iostream>

ExceptionINI::ExceptionINI(int ierrorClass, std::string ierrorMsg)
{
	errorMsg = ierrorMsg;
}

std::string ExceptionINI::getErrorMsg()
{
	return errorMsg;
}

std::string ExceptionINI::getErrorDomain()
{
	return "ExceptionINI";
}


Property::Property(std::string propertyName, std::string propertyValue)
{
	pName = propertyName;
	pValue = propertyValue;
}

std::string Property::getName()
{
	return pName;
}

std::string Property::getValue()
{
	return pValue;
}


Section::Section(std::string name, int uid)
{
	sName = name;
	sUid = uid;
}

void Section::addProperty(std::string propertyName, std::string propertyValue)
{
	sProps.push_back(Property(propertyName,propertyValue));
}

void Section::removeProperty(std::string propertyName)
{
	for (std::vector<Property>::iterator prop = sProps.begin(); prop != sProps.end(); prop++)
	{
		if (prop->getName() == propertyName)
		{
			sProps.erase(prop);
			return;
		}
	}
	throw ExceptionINI(1,std::string("(Section::removeProperty) Property: ")+propertyName+std::string(" does not exist"));
}

std::string Section::getName()
{
	return sName;
}

int Section::getUID()
{
	return sUid;
}

int Section::getPropertyCount()
{
	return sProps.size();
}

Property* Section::getPropertyIndex(int index)
{
	return &(sProps[index]);
}

Property* Section::getProperty(std::string propertyName)
{
	for (std::vector<Property>::iterator prop = sProps.begin(); prop != sProps.end(); prop++)
	{
		if (prop->getName() == propertyName)
			return &(*prop);
	}
	throw ExceptionINI(1,std::string("(Section::getProperty) Property: ")+propertyName+std::string(" does not exist"));
}


ParserINI::ParserINI(std::string iniFile)
{
	ifile = iniFile;
	uid = 0;
}

void ParserINI::parse()
{
	sections.clear();
	
	ifs.open(ifile.c_str());
	if (ifs.is_open() == false)
		throw ExceptionINI(0,std::string("(ParseINI::parse) failed to open: ")+ifile);
	
	std::string line, curSection;
	int start, end, suid = -1;
	
	while(ifs.eof() == false)
	{
		std::getline(ifs,line);
		if ((start=line.find_first_of('[')) != std::string::npos)
		{
			end = line.find_first_of(']');
			curSection = line.substr(start+1,end-start-1);
			suid = this->addSection(curSection);
		}
		else if ((start=line.find_first_of('=')) != std::string::npos)
		{
			if (suid == -1)
				throw ExceptionINI(1,std::string("(ParserINI::parse) Property with no section: ")+line);
				
			this->addProperty(curSection,line.substr(0,start),line.substr(start+1,line.length()),suid);
		}
	}
	ifs.close();
}

int ParserINI::addSection(std::string sectionName)
{
	sections.push_back(Section(sectionName,uid));
	uid++;
	if (uid == 0xFFFFFFFE)
		uid = 0;
	return uid-1;
}

void ParserINI::addProperty(std::string sectionName, std::string propertyName, std::string propertyValue, int suid)
{
	if (suid == -1)
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getName() == sectionName)
			{
				i->addProperty(propertyName,propertyValue);
				return;
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::addProperty) Section: ")+sectionName+std::string(" does not exist"));
	}
	else
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getUID() == suid && i->getName() == sectionName)
			{
				i->addProperty(propertyName,propertyValue);
				return;
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::addProperty.suid) Section: ")+sectionName+std::string(" does not exist"));
	}
}

void ParserINI::removeSection(std::string sectionName, int suid)
{
	if (suid == -1)
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getName() == sectionName)
			{
				i = sections.erase(i);
			}
		}
	}
	else
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getUID() == suid && i->getName() == sectionName)
			{
				sections.erase(i);
				return;
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::removeSection.suid) Section: ")+sectionName+std::string(" does not exist"));
	}
}

void ParserINI::removeProperty(std::string sectionName, std::string propertyName, int suid)
{
	if (suid == -1)
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getName() == sectionName)
			{
				i->removeProperty(propertyName);
				return;
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::removeProperty) Section: ")+sectionName+std::string(" does not exist"));
	}
	else
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getUID() == suid && i->getName() == sectionName)
			{
				i->removeProperty(propertyName);
				return;
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::removeProperty.suid) Section: ")+sectionName+std::string(" does not exist"));
	}
}

std::string ParserINI::getProperty(std::string sectionName, std::string propertyName, int suid)
{
	if (suid == -1)
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getName() == sectionName)
			{
				return i->getProperty(propertyName)->getValue();
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::getProperty) Section: ")+sectionName+std::string(" does not exist"));
	}
	else
	{
		for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
		{
			if (i->getUID() == suid && i->getName() == sectionName)
			{
				return i->getProperty(propertyName)->getValue();
			}
		}
		throw ExceptionINI(1,std::string("(ParserINI::getProperty.suid) Section: ")+sectionName+std::string(" does not exist"));
	}
}

std::vector<Section> ParserINI::getSections(std::string sectionClass)
{
	std::vector<Section> sects;
	
	for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
	{
		if (i->getName() == sectionClass)
		{
			sects.push_back(*i);
		}
	}
	
	return sects;
}

Section* ParserINI::getSectionByProperty(std::string propertyName, std::string propertyValue)
{
	for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
	{
		for (int p = 0; p < i->getPropertyCount(); p++)
		{
			if (i->getPropertyIndex(p)->getName() == propertyName)
			{
				if (i->getPropertyIndex(p)->getValue() == propertyValue)
					return &(*i);
			}
		}
	}
	throw ExceptionINI(0,(std::string("No Section with property '")+=propertyName+=std::string("' matching '")+=propertyValue+=std::string("'")));
}

int ParserINI::getSectionCount()
{
	return sections.size();
}

int ParserINI::getSectionCountClass(std::string sectionClass)
{
	int count = 0;
	for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
	{
		if (i->getName() == sectionClass)
			count++;
	}
	return count;
}

Section* ParserINI::getSectionBySUID(int suid)
{
	for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
	{
		if (i->getUID() == suid)
			return &(*i);
	}
	throw ExceptionINI(0,(std::string("No Section with SUID: ")+=suid));
}

void ParserINI::writeChanges()
{
	ofs.open(ifile.c_str());
	
	for (std::list<Section>::iterator i = sections.begin(); i != sections.end(); i++)
	{
		ofs << "[" << i->getName() << "]\n";
		for (int i2 = 0; i2 < i->getPropertyCount(); i2++)
		{
			ofs << i->getPropertyIndex(i2)->getName() << "=" << i->getPropertyIndex(i2)->getValue() << "\n";
		}
		ofs << "\n";
	}
	ofs.close();
}

std::string ParserINI::removeWhitespace(std::string str)
{
	std::string out = "";
	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] != ' ')
			out += str[i];
	}
	return out;
}

