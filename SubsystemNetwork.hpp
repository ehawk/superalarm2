#ifndef _SUBSYSTEM_NETWORK_HPP_
#define _SUBSYSTEM_NETWORK_HPP_

#include <stdint.h>
#include <string>

#include "Exception.hpp"
#include "Schedular.hpp"
#include "ParserINI.hpp"
#include "SubsystemSound.hpp"

#define MAGIC_NUMBER 0x4D524C41

#define CMD_UPDATE			100
#define CMD_LISTALARMS		200
#define CMD_GET				202
#define CMD_DELETEID		204
#define CMD_LISTSOUND		300
#define CMD_ADDALARM		400
#define CMD_ALARMSTATUS		402
#define	CMD_SETSOUND		500
#define	CMD_RESET			0

#define TYPE_MONOTONIC		0
#define	TYPE_CONTINUOUS		1

#define ACTION_SOUND		0
#define ACTION_NOTIFY		1
#define	ACTION_EMAIL		2
#define ACTION_EXTERNAL		3

#define STATE_ACTIVE		0
#define STATE_MISSED		1
#define STATE_DISABLED		2
#define STATE_TRIGGERED		3

typedef struct
{
	uint32_t 	Magic;
	uint32_t 	Command;
	uint32_t 	Flags;
	uint32_t	Size;
	uint8_t*	Data;
} Packet;

typedef struct
{
	uint32_t	Epoch;
	uint32_t	Period;
	uint32_t	Identifier;
	uint8_t		State;
	uint8_t		Type;
	uint8_t		Action;
	uint8_t		NameLength;
	uint8_t*	Name;
	
} AlarmDef;

class ExceptionSubsystemNetwork : public Exception
{
	public:
		ExceptionSubsystemNetwork(std::string msg);
		std::string getErrorMsg();
		std::string getErrorDomain();
		
	private:
		std::string esnMsg;
};

class ICommandInterface
{
	public:
		virtual void CommandAddAlarm(uint8_t* newAlarm) = 0;
		virtual void CommandDeleteAlarm(uint32_t* index) = 0;
		virtual void CommandGetAlarm(uint32_t* index) = 0;
		virtual void CommandListAlarms() = 0;
		virtual void CommandListSounds() = 0;
		virtual void CommandSetSound(uint8_t* data) = 0;
};

class SubsystemNetwork : public IJob
{
	public:
		SubsystemNetwork(Schedular* s, ICommandInterface* com, int port);
		~SubsystemNetwork();
		void sendPacket(uint8_t* data, uint16_t size, int command);
		void enable();
		
		struct pollfd getDescriptor();
		bool onReady();
		std::string getName();
		unsigned int getIdentifier();
		bool isVolatile();
		
	private:
		Schedular* snSchedular;
		ICommandInterface* snCom;
		int snListener;
		int snClientSocket;
};

#endif
