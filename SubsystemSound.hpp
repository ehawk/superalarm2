#ifndef _SUBSYSTEM_SOUND_HPP_
#define _SUBSYSTEM_SOUND_HPP_

#include "Schedular.hpp"
#include "Exception.hpp"
#include <stdint.h>
#include <alsa/asoundlib.h>
#include <memory>

#define PCM_PERIOD 4096

class ExceptionSubsystemSound : public Exception
{
	public:
		ExceptionSubsystemSound(std::string msg);
		~ExceptionSubsystemSound();
		std::string getErrorMsg();
		std::string getErrorDomain();
		
	private:
		std::string eMsg;
};

typedef struct
{
	uint32_t	chunkMagic;
	uint32_t	chunkLength;
	uint32_t	chunkTag;
} RIFFHeader;

typedef struct
{
	uint32_t	wavMagic;
	uint32_t	wavLength;
	uint16_t	wavFormat;
	uint16_t	wavChannels;
	uint32_t	wavFrequency;
	uint32_t	wavByteRate;
	uint16_t	wavBlockAlign;
	uint16_t	wavBitDepth;
} WAVFormat;

typedef struct
{
	uint32_t	wavMagic;
	uint32_t	wavLength;
} WAVSamples;

class DecoderWAV
{
	public:
		DecoderWAV(std::string fileName);
		~DecoderWAV();
		int getChannels();
		int getFrequency();
		int getBitDepth();
		snd_pcm_format_t getFormat();
		bool isDone();
		char* getSamples(int offset, int size);
		std::string getName();
		
	private:
		std::unique_ptr<char> samples;
		std::unique_ptr<char> end;
		uint32_t samplesCount;
		WAVFormat fmt;
		bool done;
		std::string dwName;
};

class SubsystemSound : public IJob
{
	public:
		SubsystemSound(Schedular* s);
		~SubsystemSound();
		void playWav(std::string wavFile);
		
		struct pollfd getDescriptor();
		bool onReady();	
		std::string getName();
		unsigned int getIdentifier();
		bool isVolatile();
		
	private:
		std::auto_ptr<snd_pcm_t> ssHnd;
		std::auto_ptr<DecoderWAV> ssWav;
		Schedular* ssSchedular;
		int offset;
		bool isPlaying;
		
	friend class JobBufferFill;
};

#endif
