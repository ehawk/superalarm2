#ifndef _PARSER_INI_HPP_
#define _PARSER_INI_HPP_

#include "Exception.hpp"

#include <string>
#include <fstream>
#include <vector>
#include <list>

class ExceptionINI : public Exception
{
	public:
		ExceptionINI(int errorClass, std::string errorMsg);
		std::string getErrorMsg();
		std::string getErrorDomain();
		
	private:
		std::string errorMsg;
};

class Property
{
	public:
		Property(std::string propertyName, std::string propertyValue);
		std::string getName();
		std::string getValue();
		
	private:
		std::string pName;
		std::string pValue;
};

class Section
{
	public:
		Section(std::string name, int uid);
		void addProperty(std::string propertyName, std::string propertyValue);
		void removeProperty(std::string propertyName);
		std::string getName();
		int getUID();
		int getPropertyCount();
		Property* getProperty(std::string propertyName);
		Property* getPropertyIndex(int index);
		
	private:
		std::string sName;
		std::vector<Property> sProps;
		int sUid;
};

class ParserINI
{
	public:
		ParserINI(std::string iniFile);
		
		void parse();
		int addSection(std::string sectionName);
		void addProperty(std::string sectionName, std::string propertyName, std::string propertyValue, int suid = -1);
		void removeSection(std::string sectionName, int suid = -1);
		void removeProperty(std::string sectionName, std::string propertyName, int suid = -1);
		std::string getProperty(std::string sectionName, std::string propertyName, int suid = -1);
		std::vector<Section> getSections(std::string sectionClass);
		Section* getSectionByProperty(std::string propertyName, std::string propertyValue);
		Section* getSectionBySUID(int suid);
		void writeChanges();
		int getSectionCount();
		int getSectionCountClass(std::string sectionClass);
		
	private:
		std::string removeWhitespace(std::string str);
	
		std::ifstream ifs;
		std::ofstream ofs;
		std::list<Section> sections;
		int uid;
		std::string ifile;
};

#endif
