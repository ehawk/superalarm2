FLAGS=
N_CFLAGS = 
LIBS=-lpthread -lrt -lm -ldl
OBJECTS := $(patsubst %.cpp,%.o,$(wildcard *.cpp))


all: $(OBJECTS)
	$(CXX) $(FLAGS) -lasound -o superalarm $(OBJECTS)

%.o: %.cpp
	$(CXX) $(FLAGS) -std=c++0x -g -c -o $@ $<
	
