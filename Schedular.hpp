#ifndef _DISPATCHER_HPP_
#define _DISPATCHER_HPP_

#include "Exception.hpp"

#include <poll.h>
#include <string>
#include <vector>
#include <memory>

class ExceptionSchedular : public Exception
{
	public:
		ExceptionSchedular(std::string msg);
		std::string getErrorMsg();
		std::string getErrorDomain();
		
	private:
		std::string eMsg;
};

class IJob
{
	public:
		virtual struct pollfd getDescriptor() = 0;
		virtual bool onReady() = 0;
		virtual std::string getName() = 0;
		virtual unsigned int getIdentifier() = 0;
		virtual bool isVolatile() = 0;
		virtual ~IJob() {}
};

class Schedular
{
	public:
		Schedular();
		~Schedular();
		void addJob(IJob* job);
		void removeJob(IJob* job);
		void removeJob(unsigned int id);
		bool waitForJob();
		
	private:
		void rebuildJobList();
		void runNextTask();
		
		bool rebuildNeeded;
		bool inLoop;
		std::vector< std::unique_ptr<IJob> > jobs;
		std::vector< std::unique_ptr<IJob> > queuedJobs;
		struct pollfd* pfd;
		
};

#endif
